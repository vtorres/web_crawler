#!/usr/bin/env python
# -*- encoding: utf-8 -*-

"""
Web crawler that prints the sitemap of a given URL, as well as the assets
that are part of each page.

Usage:

$ ./web_crawler.py URL
"""
import sys

__author__ = 'Vitor Torres'
__license__ = 'GPL'
__version__ = '0.0.1'
__email__ = 'vitor.torres@gmail.com'


import argparse
import traceback
import requests
from urlparse import urlparse

from bs4 import BeautifulSoup
from lxml.etree import XMLSyntaxError
from requests.packages import urllib3


class WebCrawler():
    url = None
    visited = []
    output_file = None

    def __init__(self, url, output_file=None):
        self.url = url
        if self.url[::-1] != '/':
            self.url = '%s/' % self.url
        if not self.url_is_alive():
            print 'URL is invalid or website is down'
            sys.exit(1)
        self.output_file = output_file
        # if the website in under HTTPS, we we'll be annoyed with some warnings
        # here, we disabling them
        urllib3.disable_warnings()

    def url_is_alive(self):
        """
        Simple check to see if the website is alive and kicking
        """
        try:
            request = requests.get(self.url)
        except requests.exceptions.ConnectionError:
            return False
        return request.status_code == 200

    def crawl(self):
        self.visited = self._crawl()
        if self.output_file:
            self.save_results_to_file()
        else:
            self.print_results()

    def _crawl(self, url=None, path=None, visited=[]):
        """
        This function will traverse all internal urls, and gather all assets
        """
        if not url:
            url = self.url
        soup = self.get_soup(url)
        anchors = soup.findAll('a', href=True)
        links = self.get_local_links(anchors)
        # visited helps us keep track of what pages we already visited, so
        # we don't waste time and processing power revisiting urls
        visited.append({
            'url': url,
            'path': path,
            'assets': {
                'css': self.get_css_assets(soup),
                'js': self.get_js_assets(soup),
                'imgs': self.get_img_assets(soup),
                'rss': self.get_rss_assets(soup),
                'external_links': self.get_external_links(soup),
            }
        })
        for link in links:
            # for all the links we can find inside this page,
            # if we haven't visited it already, we call _crawl again
            # to repeat the process on the unvisited page
            url = '%s%s' % (self.url, link)
            if not filter(lambda x: x['url'] == url, visited):
                self._crawl(url, link, visited)
        return visited

    def get_soup(self, url):
        html = self.get_html_from_url(url)
        try:
            soup = BeautifulSoup(html, 'lxml')
        except XMLSyntaxError:
            # lxml parser error, let's try another one
            soup = BeautifulSoup(html, 'html.parser')
        return soup


    def get_html_from_url(self, url):
        """
        Attempts to get the HTML for the url passed. A number of attempts will be made.
        If nothing can be found, None will be returned.
        """
        html = None
        default_agent = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_11_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.86 Safari/537.36'

        parsed_uri = urlparse(url)
        if not parsed_uri.scheme:
            url_to_fetch = 'http://{uri.path}'.format(uri=parsed_uri)

        if url:
            try:
                response  = requests.get(url, headers={'User-Agent': default_agent})
                if response.status_code == 403:
                    return None
                html = response.text
            except Exception, e:
                error = traceback.format_exc()
        return html

    def _is_invalid_internal_link(self, href):
        return href == '' or href.startswith('#') or href.startswith('http') and not href.startswith(self.url) \
            or href.lower().startswith('mailto:') or href.lower().endswith('.zip') or href.startswith(self.url) \
            or href.find('../') > -1 or href.startswith('tel:')

    def get_local_links(self, anchors):
        links = []
        for anchor in anchors:
            href = anchor['href']
            # List of url variants that the crawler can skip
            if not self._is_invalid_internal_link(href):
                if href.find('#') > 0:
                    # if an url contains the # symbol we just get the url until #
                    links.append(href[:href.index('#')])
                else:
                    links.append(href)
        return set(links) # in case we have repeated links, we transform our list in a set

    def get_css_assets(self, soup):
        """
        Gets all css files used by a page
        """
        return [link["href"] for link in soup.findAll("link") if "stylesheet" in link.get("rel", [])]

    def get_js_assets(self, soup):
        """
        Gets all scripts inside a page. We don't want inline scripts.
        """
        return [script["src"] for script in soup.findAll("script") if script.get('src', None)]

    def get_img_assets(self, soup):
        """
        Gets all the images inside a page.
        """
        return [image["src"] for image in soup.findAll("img")]

    def get_rss_assets(self, soup):
        """
        Gets all RSS links inside a page.
        """
        return [rss["href"] for rss in soup.findAll("link") if "application/atom+xml" in rss.get("rel", [])]

    def get_external_links(self, soup):
        """
        Gets all the external links inside a page.
        """
        return [anchor["href"] for anchor in soup.findAll("a", href=True) if anchor['href'].startswith('http') and not anchor['href'].startswith(self.url)]

    def get_report(self):
        """
        Generating the report as a list of strings to be then used by
        the print or save to file functionalities
        """
        text = []
        text.append('***** Results for %s *****' % self.url)
        text.append('###########')
        text.append('# Sitemap #')
        text.append('###########')

        for link in self.visited:
            text.append('* %s' % link['path'])
        text.append('') # just an empty line
        text.append('###########')
        text.append('# Sitemap #')
        text.append('###########')
        text.append('') # just an empty line
        for link in self.visited:
            if any([link['assets']['css'], link['assets']['js'], link['assets']['imgs'], link['assets']['rss'], link['assets']['external_links']]):
                text.append('** Assets on %s' % link['path'])
                if link['assets']['css']:
                    text.append('-- CSS Assets --')
                    for css in link['assets']['css']:
                        text.append('    - %s' % css)
                if link['assets']['js']:
                    text.append('-- JS Assets --')
                    for js in link['assets']['js']:
                        text.append('    - %s' % js)
                if link['assets']['imgs']:
                    text.append('-- IMG Assets --')
                    for img in link['assets']['imgs']:
                        text.append('    - %s' % img)
                if link['assets']['rss']:
                    text.append('-- RSS Assets --')
                    for rss in link['assets']['rss']:
                        text.append('    - %s' % rss)
                if link['assets']['external_links']:
                    text.append('-- External links Assets --')
                    for link in link['assets']['external_links']:
                        text.append('    - %s' % link)
        return text

    def print_results(self):
        """
        Print results to stout
        """
        report = self.get_report()
        print '\n'.join(report)

    def save_results_to_file(self):
        f = open(self.output_file, 'w')
        report = self.get_report()
        f.writelines('\n'.join(report))
        f.close()


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='A Web crawler that prints the sitemap of a given URL, as well as the assets that are part of each page.'
    )
    parser.add_argument('URL')
    parser.add_argument('-o', '--output', help='Save the report to a file. If none is set, report will be printed to your screen')

    args = parser.parse_args()

    if args.URL:
        wc = WebCrawler(args.URL, args.output)
        wc.crawl()
