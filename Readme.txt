Candidate task - Vitor Torres

First:
$ git clone git@bitbucket.org:vtorres/web_crawler
$ cd web_crawler
$ virtualenv --no-site-packages env
$ source env/bin/activate
$ pip install -r requirements.txt
$ ./web_crawler.py -h
